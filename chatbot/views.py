from django.shortcuts import HttpResponse
import json
from .pinyin2chinese import spell2chinese
from .tencent import chat


def pinyin2chinese(request):
    data = json.loads(request.body)
    question = data.get('info')
    question = spell2chinese(question)
    info = dict()
    info['question'] = question
    return HttpResponse(json.dumps(info), content_type="application/json")


def chat_bot(request):
    data = json.loads(request.body)
    question = data.get('question')
    return HttpResponse(json.dumps({'answer': chat(question)}), content_type="application/json")

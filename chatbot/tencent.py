import requests as rq
import string
import random
import hashlib
import urllib
import time


def get_sign(data):
    lst = [i[0] + '=' + urllib.parse.quote_plus(str(i[1])) for i in data.items()]
    params = '&'.join(sorted(lst))
    s = params + '&app_key=f7yYhoj55T9gnpuO'
    h = hashlib.md5(s.encode('utf8'))
    return h.hexdigest().upper()


def chat(question):
    url_chat = 'https://api.ai.qq.com/fcgi-bin/nlp/nlp_textchat'
    data = {
        'app_id': 2115827064,  # 换成你的app_id
        'time_stamp': int(time.time()),
        'nonce_str': ''.join(random.sample(string.ascii_letters + string.digits, 16)),
        'session': '10000',
        'question': question,
    }
    data['sign'] = get_sign(data)
    r = rq.post(url_chat, data=data)
    answer = r.json()['data']['answer']
    if answer is None or answer == '':
        answer = '风太大，我听不清！！'
    return answer

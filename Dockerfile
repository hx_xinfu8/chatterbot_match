FROM python:3.6
MAINTAINER hx
RUN mkdir -p /home/app
ADD . /home/app
WORKDIR /home/app
RUN pip install -r requirements.txt
ENV HOST 0.0.0.0
ENV PORT 8000
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]